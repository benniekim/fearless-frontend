window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();

      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }
      // Here, add the 'd-none' class to the loading icon
      const divTag = document.getElementById('loading-conference-spinner');
      const dNoneClass = ["d-none"];
      divTag.classList.add(dNoneClass);

      // Here, remove the 'd-none' class from the select tag
      selectTag.classList.remove(dNoneClass);

      const formTag = document.getElementById('create-attendee-form');
            formTag.addEventListener('submit', async event => {
                event.preventDefault();
                const formData = new FormData(formTag);
                const json = JSON.stringify(Object.fromEntries(formData));
                const attendeeUrl = 'http://localhost:8001/api/attendees/';
                const fetchConfig = {
                    method: "post",
                    body: json,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                };
                const response = await fetch(attendeeUrl, fetchConfig);
                if (response.ok) {
                    // Here, add the 'd-none' class to the form
                    const form = document.getElementById('create-attendee-form');
                    const dNoneClass = ["d-none"];
                    form.classList.add(dNoneClass);
                    // Here, remove the 'd-none' class from the select tag
                    const succesMessage = document.getElementById('success-message');
                    succesMessage.classList.remove(dNoneClass);
                    formTag.reset();
                    const newAttendee = await response.json();
                    console.log(newAttendee);
                }
            });
    }

  });
