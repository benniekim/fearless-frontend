function createCard(title, description, pictureUrl, location, starts, ends) {
    return `
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${title}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
            ${starts} - ${ends}
        </div>
      </div>
    `;
  }

function alert() {
    return`
        <div class="alert alert-warning d-flex align-items-center" role="alert">
        <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Warning:"><use xlink:href="#exclamation-triangle-fill"/></svg>
        <div>
            Error: bad ! !
        </div>
        </div>
    `;
}

  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
    const columns = document.querySelectorAll(".col");
    let colIndx = 0;

    try {
      const response = await fetch(url);

      if (!response.ok) {
        alert

      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const location = details.conference.location.name;
            const starts = new Date(details.conference.starts).toDateString();
            const ends = new Date(details.conference.ends).toDateString();
            const html = createCard(title, description, pictureUrl, location, starts, ends);

            const column = columns[colIndx % 3];
            column.innerHTML += html;
            colIndx = (colIndx + 1) % 3
          }
        }

      }
    } catch (e) {
        alert
    }

  });
